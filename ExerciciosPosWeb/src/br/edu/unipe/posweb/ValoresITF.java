/**
 * 
 */
package br.edu.unipe.posweb;

/**
 * @author wilson
 *
 */
public interface ValoresITF {
	
	boolean ins(int v);  //insere valor
	
	int del(int i); //delata valor
	
	int size(); //retorna quantidade valor armazenado.
	
}