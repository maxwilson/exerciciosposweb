package br.edu.unipe.posweb;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestValores {
	
	private Valores val;
	
	
	@Test
	public void testIns() {
		
		val = new Valores();
		
		assertEquals(false, val.ins(-8));
		assertEquals(true, val.ins(9));
		assertEquals(true, val.ins(2));
		val.ins(2);
		
	}
	
	@Test
	public void testDel() {
		val = new Valores();
		assertEquals(-1, val.del(0));
	}
	
	@Test
	public void test() {
		val = new Valores();
		val.ins(5);
		val.ins(12);
		val.ins(10);
		assertEquals(3, val.size());
	}
	
	@Test
	public void delFail() {
		val = new Valores();
		assertEquals(-1, val.del(-6));
	}

}
