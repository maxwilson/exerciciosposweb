<project name="ExerciciosPosWeb" default="main" basedir=".">
	<description>project</description>
	<!-- global properties for this build file -->
	<property name="source.dir" location="src" />
	<property name="build.dir" location="bin" />
	<property name="doc.dir" location="doc" />
	<property name="testreport.dir" location="testreport" />
	<property name="test.build.dir" value="build/test"/>
	<property name="test.src.dir" value="src"/>
	<!-- set up some directories used by this project -->
	<target name="init" description="setup project directories">
		<mkdir dir="${build.dir}" />
		<mkdir dir="${doc.dir}" />
		<mkdir dir="${testreport.dir}"/>
	</target>
	<!-- Compile the java code in ${src.dir} into ${build.dir} -->
	<target name="compile" description="compile java sources">
		<javac srcdir="${source.dir}" destdir="${build.dir}">
			<classpath refid="junit.class.path" />
		</javac>
	</target>
	<!-- Generate javadocs for current project into ${doc.dir} -->
	<target name="doc" description="generate documentation">
		<javadoc sourcepath="${source.dir}" destdir="${doc.dir}" />
	</target>
	<!-- Test com junit e relatorio -->
	<junitreport todir="${testreport.dir}">
		<fileset dir="${testreport.dir}">
			<include name="REPORTS-*.xml"/>
		</fileset>
		<report format="frames" todir="${testreport.dir}/html"/>
	</junitreport>
	
	
	<property name="main.build.dir" value="build/main"/>
	<property name="main.src.dir" value="src"/>
	<property name="test.src.dir" value="src/br/edu/unipe/posweb/TestValores.java"/>
	
	<path id="classpath.test">
		<pathelement location="lib/junit-4.13.jar"/>
		<pathelement location="lib/hamcrest-core-1.3.jar"/>
		<pathelement location="${main.build.dir}"/>
	</path>
	
	
	<target name="compile2">
		<mkdir dir="${main.build.dir}"/>
		<javac srcdir="${main.src.dir}" destdir="${main.build.dir}" includeantruntime="false"/>
	</target>
	
	<target name="test-compile" depends="compile2">
		<mkdir dir="${test.build.dir}"/>
		<javac srcdir="${test.src.dir}" destdir="${test.build.dir}" includeantruntime="false">
			<classpath refid="classpath.test"/>
		</javac>
	</target>
	
	<target name="test" depends="test-compile">
		<junit printsummary="on" haltonfailure="yes" fork="true">
			<classpath>
				<path refid="classpath.test"/>
				<pathelement location="${test.build.dir}"/>
			</classpath>
			<formatter type="brief" usefile="false" />
			<batchtest>
				<fileset dir="${test.src.dir}" includes="**/*Test.java" />
			</batchtest>
		</junit>
	</target>
	
	<target name="jar">
		<mkdir dir="${build.dir}" />
		<jar destfile="jar/aplicacao.jar" basedir="${build.dir}"></jar>
	</target>
	<!--
    <target name="run"><java jar="jar/aplicacao.jar" fork="true" /></target>
    -->
	<target name="main" depends="init,jar,compile, doc,test">
		<description>Main target</description>
	</target>
</project>




<project name="ExerciciosPosWeb" default="main" basedir=".">
	<description>project</description>
	
	<!-- global properties for this build file -->
	<property name="source.dir" location="src" />
	<property name="build.dir" location="bin" />
	<property name="doc.dir" location="doc" />
	<property name="testreport.dir" location="testreport" />
	<property name="test.build.dir" value="build/test"/>
	<property name="test.src.dir" value="src"/>
	
	<property name="main.build.dir" value="build/main"/>
	
	<!-- set up some directories used by this project -->
	<target name="init" description="setup project directories">
		<mkdir dir="${build.dir}" />
		<mkdir dir="${doc.dir}" />
		<mkdir dir="${testreport.dir}"/>
		<mkdir dir="${main.build.dir}"/>
	</target>
	
	<!-- adicionando junit ao classpath -->
	<path id="junit.class.path">
		<pathelement location="lib/junit-4.12.jar" />
		<pathelement location="lib/hamcrest-core-1.3.jar" />
		<pathelement location="${build.dir}" />
	</path>
	
	<!-- Generate javadocs for current project into ${doc.dir} -->
	<target name="doc" description="generate documentation">
		<javadoc sourcepath="${source.dir}" destdir="${doc.dir}">
			<classpath refid="junit.class.path" />
		</javadoc>	
	</target>
	
	<!-- Run the JUnit Tests -->
	<!-- Output is XML, could also be plain-->
	<target name="junit">
		<junit printsummary="on" fork="true" haltonfailure="yes">
			<classpath refid="junit.class.path" />
			<classpath>
			    <pathelement location="${build.test.dir}"/>
			  </classpath>
			<formatter type="xml" />
			<batchtest todir="${test.report.dir}">
				<fileset dir="${test.dir}">
					<include name="**/*Test*.java" />
				</fileset>
			</batchtest>
		</junit>
	</target>
	
	<!-- Test com junit e relatorio -->
	<junitreport todir="${testreport.dir}">
		<fileset dir="${testreport.dir}">
			<include name="REPORTS-*.xml"/>
		</fileset>
		<report format="frames" todir="${testreport.dir}/html"/>
	</junitreport>
	
	
	<target name="jar">
		<mkdir dir="${build.dir}" />
		<jar destfile="jar/aplicacao.jar" basedir="${build.dir}"></jar>
	</target>
	<!--
    <target name="run"><java jar="jar/aplicacao.jar" fork="true" /></target>
    -->
	<target name="main" depends="init,jar,compile, doc">
		<description>Main target</description>
	</target>
</project>